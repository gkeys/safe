# Nerdalize Safe

The scenario's:

1. Your team needs **secrets** (keys, passwords) to **deploy or control** your cloud infrastructure (AWS, GCE, SimpleDNS, you name it). So your colleague stores the infrastructure secrets in **.secret files** parsed by **a Makefile** that drives Terraform. You, being much more clever, have a **superior solution in bash** and, having your `*.secret` directive in .gitignore, scp the relevant secrets to you colleague when he needs it for testing. 

2. You put your secrets on your production instance disks at **configuration time** (e.g. using ansible and ansible-vault)

3. **You are are a pro** using using **Hashicorp Vault** and possibly **Consul Templates** to dynamically configure your secrets at Runtime. Good Job! But now the existence of that secret in that environment has become a **runtime dependency**. You should **test it as such**.

You rightly believe [HashiCorp Vault](https://www.vaultproject.io/) is The Way (REST API hurray), but you're busy. It takes 3 hours to learn and 8 hours to set up for production in a HA setup with Terraform and Consul and Ansible and be ready for production (at least). So you will do it when you have time. 

Right. 

**Stop. Give me 120 seconds** to let you try Hashicorp Vault with a non-intrusive wrapper. Nerdalize Safe does 2 things with a single command: 

1. Run Vault on your laptop in Docker, which takes care of initialization/authentication/restart. The token is stored on your laptop, which is encrypted already. 
2. Provide a way to cat YAML files with your secrets to your secure, encrypted HashiCorp Vault. 

## Vault in 120 seconds

### Install

You need [Docker](https://docs.docker.com/engine/installation/) and a very short [shell script](https://gitlab.com/nerdalize/safe/raw/master/safe): 
```
sudo curl -o /usr/local/bin/safe https://gitlab.com/nerdalize/safe/raw/master/safe && \
sudo chmod +x /usr/local/bin/safe 
```

### Absorb secrets with Nerdalize Safe

Use Nerdalize Safe to start Vault and make it absorb dummy secrets (e.g. for testing in dev) from an [unencrypted YAML file](https://gitlab.com/nerdalize/safe/raw/master/dummy-secrets.yml): 

```
cat dummy-secrets.yml | safe absorb
```

### Done! Now use Vault. 

Now you can start using Vault using the [client](https://www.vaultproject.io/downloads.html), like you normally would, to list the secrets:

```
$ vault list /secret
Keys
example-prod/
example-test/
```

Query the secrets: 

```
$ vault read -format=json /secret/example-test/mysql
{
	"lease_id": "",
	"lease_duration": 2592000,
	"renewable": false,
	"data": {
		"password": "my_dummy_pass_for_dev"
	},
	"warnings": null
}
```

Develop against the [Vault API](https://www.vaultproject.io/docs/http/) running at `http://localhost:8200/`


# 

# Motivation
HashiCorp Vault is a great tool to store/access your secrets in 
Production. It provides a wealth of features for running it in a clustered production environment: 
HA, ACL's, different storage backends, secret backends that dynamically manage your AWS secrets, 
leasing, etc. It's great for Ops. 

As a developer, you love the flexibility of querying secrets with an API. But these secret paths 
(the names, note the values) are dependencies. So you need to test and stage them like you do with 
your code. But instead of a Vault, you may want something that is more lightweight, and can easily 
be carried to another environment. 

You want a simple tool, and a simple workflow. A Safe.

## Goals

Nerdalize Safe is a simple extended & Dockerized distribution of Hashicorp Vault (aiming to become obsoleted by HashiCorp Vault) that is just that bit easier to use. It helps you: 

- **Maximize dev/prod parity** by developing against a real Vaul API, while also **isolating development** to allow free experimentation.
- **Quickly add, remove and modify secrets to your local dev environment**, to test whether your software accesses the correct secret paths.
- **Put your paths & dummies in YAML and git**. You can build a structure to match your future ACL's, and share it with others.
- **Import secrets to your Safe** by catting a simple (possibly encrypted) YAML file 
- **Help your Dev co-workers**, as they can import the secret paths (and dummy secrets) with 1 command into Safe.
- **Help your Ops co-workers**, as you automatically provide them with a log of **new secrets** they should generate and add/remove to the production Hashicorp Vault. 

## Suggested workflow

### Workflow 1: Your deployment tools us Vault at deployment time

In this workflow, you **only use secrets at deploy time**. That is, you use them as input for Terraform, Ansible -- and Ansible may store the secrets on disk (Using Docker in production? Check the [security cheatsheet](http://container-solutions.com/docker-security-cheat-sheet/)). 

You don't want the hassle of full ACL and centrally managed Vault operations, so you 

* encrypt your secret file with your favourite encryption tool (in the example we assume [ansible-vault](http://docs.ansible.com/ansible/playbooks_vault.html), which is a simple solution based on a shared secret) 
* commit your encrypted yml file(s) to a single git repo all your production secrets
* share the secret very carefully
* trusted parties can load the secrets with `ansible-vault decrypt --output=- dummy-secrets.crypt | safe absorb` (the password of the example file is `abc` -- note that the stdout is mixed, but the prompt is waiting for input)
* Run `vault list /secret` , and notice you now have an `example-prod` secret environment as well. 

### Workflow 2: Your applications use Vault at runtime
If your applications query the Vault API at runtime. In this case, you should run a central Vault server, but you can benefit from being able to quickly add/remove secrets for dev stage, and share them. 

* put dummy secrets (paths+values) that your applications depend on as unencrypted .yml files in the git repo of the relevant software package. 
* commit them so everybody can use them for testing
* cat the files with `cat dummy-secrets.yml | safe absorb` when you test the code. 



### Build the container yourself

```
make build # build container
make install # install the `safe' script
```

## Ansible Plugin

Please use Ansible 2.0 

* if [this PR](https://github.com/ansible/ansible/pull/13690) is merged , use the relevant ansible branch
* otherwise, override the hashi_vault plugin with [this version](https://github.com/feliksik/ansible/blob/hashivault/lib/ansible/plugins/lookup/hashi_vault.py)


