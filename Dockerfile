# Docker file to run Hashicorp Vault locally. 
#

FROM ubuntu:15.10
MAINTAINER Eric Feliksik <e.feliksik@nerdalize.com>

ENV VAULT_VERSION 0.5.1
ENV VAULT_TMP /tmp/vault.zip
ENV VAULT_HOME /usr/local/bin
ENV PATH $PATH:${VAULT_HOME}

ENV VAULT_ADDR http://127.0.0.1:8200

RUN apt-get update && apt-get install -y \
      curl \
      jq \
      coreutils \
      wget \
      unzip \
      bash \
      ca-certificates \
      python python-requests python-yaml

RUN wget --quiet --output-document=${VAULT_TMP} https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip && \
    unzip ${VAULT_TMP} -d ${VAULT_HOME} && rm -f ${VAULT_TMP}

ADD dev-server.hcl /root/dev-server.hcl
ADD deps/bin/yaml-go-json /usr/local/bin/yaml-go-json

ADD manage-vault /usr/local/bin/manage-vault
ADD scripts/vault-upload /usr/local/bin/vault-upload
ADD scripts/vault-secrets-flatten.py /usr/local/bin/vault-secrets-flatten.py
ADD scripts/vault-secrets-flat-write.py /usr/local/bin/vault-secrets-flat-write.py

# Listener API tcp port
EXPOSE 8200

CMD ["manage-vault", "server"]

#ENTRYPOINT ["/usr/local/bin/vault"]
#CMD ["server", "-config", "/root/dev-server.hcl"]

