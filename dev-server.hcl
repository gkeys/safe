backend "file" {
  address = "127.0.0.1:8500"
  path = "/data/secrets"
}


listener "tcp" {
  # if running in a docker container, we assume you run with -p 127.0.0.1:8200:8200 . 
  # that means you are actually only binding to 127.0.0.1, even if your are binding to 
  # 0.0.0.0 in the container. 
  # Alternatively, you can set address = 127.0.0.1 and start the container with --net=host
  address = "0.0.0.0:8200"
  tls_disable = 1
}

