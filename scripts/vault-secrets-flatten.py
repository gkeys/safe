#!/usr/bin/env python

description="""transform a nested dictionary (yaml/json) into a key-value pairs, 
where the values are data-dictionaries for Vault"""

import argparse

import yaml
import sys

import json

def transform(inputdict, prefix="", targetdict={}):
    """merge nested dict until the last dict (where the values are strings) 
    so we support nicely nested dicts, which are converted to paths. 
    
    string is returned as-is.
"""
    assert (type(inputdict)==dict)
    
    if len(inputdict)==0:
        targetdict[prefix] = inputdict
        return targetdict
        
    # gather all fiels with v!=dict
    primitive_fields = dict([k,v] for k,v in inputdict.iteritems() if type(v)!=dict and type(v)!=list)
    
    # the primitive key/value pairs are directly absorbed in target
    if len(primitive_fields)>0:
        targetdict[prefix] = primitive_fields
    
    dict_fields = dict([k,v] for k,v in inputdict.iteritems() if type(v)==dict)
    
    for k,v in dict_fields.iteritems():
        assert(type(v)==dict)
        d = transform(v, prefix+'/'+k, {})
        targetdict.update(d)
        
    result = primitive_fields.update({})

    return targetdict #transformed

def die(s):
        sys.stderr.write("Error: %s\n" % s)
        sys.exit(1)
            
def main():

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('inputfile', metavar='N', type=str, help='yaml input file')
    parser.add_argument('--test', action='store_true', help='run unit test (inputfile is required but ignored)')
    parser.add_argument('--format', '-f', type=str , default='json', help='output format (yaml|json)')
    
    args = parser.parse_args()
    if args.test:
        test()
        print('tests passed')
        exit()
        
        
    d = None
    inputfile = args.inputfile
    if inputfile=="-":
      inputfile='/dev/stdin'
      
    with open(inputfile, 'r') as stream:
        try:
            d = yaml.load(stream)
        except yaml.scanner.ScannerError as e:  
            die("Error reading/parsing '%s': %s" % (inputfile, e))
        except IOError as e:
            die("Cannot open secretfile '%s': %s" % (inputfile, e))
        
    if type(d)!=dict:
        die("The file %s does not seem to contain a yaml dictionary." % inputfile)
    
    transformed = transform(d)
    if args.format=='yaml':
        serialized = yaml.dump(transformed, default_flow_style=False)
    else: 
        serialized = json.dumps(transformed, 
            sort_keys=True,
            indent=4, 
            separators=(',', ': ')
        )
    
    print(serialized)

def testTransform(testname, d_in, d_out_expected):
    d_out = transform(d_in, '')
    
    if d_out != d_out_expected:
        print("ERROR in test '%s'" % testname)
        print("input: \n\t%s" % str(d_in))
        print("output: \n\t%s" % str(d_out))
        print("expected output: \n\t%s" % str(d_out_expected))
        sys.exit(1)
        
def test():
            
    testTransform("respect last dict (do not fold it)",
        { 
            'k2': {
              'k3': 'v3',
              'k4': 'v4' 
            }
        }, 
        {
            '/k2': {
              'k3': 'v3',
              'k4': 'v4' 
            }
        }
    )
    
    testTransform("complex transform",
        { # input
            'k2': {
                'k3': 'v3',
                'k4': {
                  'k5': 'v5',
                  'k6': 'v6',
                  'empty' : {
                  },
                  'deep' : { 
                    'k7':'v7'
                  }
                }
            }
        },
        { # output
           '/k2': {'k3': 'v3'},
           '/k2/k4': {
                  'k5': 'v5',
                  'k6': 'v6',
           },
           '/k2/k4/empty': {},
           '/k2/k4/deep': {
                  'k7': 'v7'
           },
        }
    )
    
    
    
        
if __name__=="__main__":
    main()
    
