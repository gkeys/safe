#!/bin/bash

envs="dev acc prod"
rw="read write"


create_policy(){
	rwmode="$1"
	env_prefix="$2"
        echo "generating policy '$env_prefix-$rwmode'..."
        # write policy to file policies/$env_prefix
        cat >policies/$env_prefix-$rwmode.acl << EndOfMessage
			path "secret/$env_prefix/*" {
			  policy = "$rwmode"
			}
			path "auth/token/lookup-self" {
			  policy = "read"
			}
EndOfMessage


            #    echo $POLICY | vault policy-write $env_prefix -
}

write_policy(){
	policy_name="$1"
	policy_file="$2"
        echo "writing policy '$policy_name'..."
	vault policy-write "secret/$policy_name" $policy_file
}

create(){ # generate policies in the policy/ directory

    mkdir -p policies
    echo $rw | tr ' ' '\n' | while read rwmode; do
        echo $envs | tr ' ' '\n' | while read env_prefix; do
            create_policy $rwmode $env_prefix
        done
    done

}

write(){ # upload policies to vault; assumes VAULT_ADDR is set correctly
    echo $rw | tr ' ' '\n' | while read rwmode; do
        echo $envs | tr ' ' '\n' | while read env_prefix; do
            write_policy "$env_prefix-$rwmode" policies/$env_prefix-$rwmode.acl
        done
    done
}





help(){ # show this help
        echo "$(basename $0) -- $DESCRIPTION"
        echo ""
        echo "usage: $0 SUBCOMMAND [OPTIONS]"
        echo ""
        echo "SUBCOMMAND: "
        # expose the functions that have a documentation comment
        grep -e "^[a-z_]*(){\s*#" $0 | sed 's/^/\t/' | sed 's/(){\s*#/\t\t/g'
        exit 1
}

die(){
  echo "ERROR: $1" >/dev/stderr
  exit 1
}

run_subcommand(){
        case $1 in
                --help|-*) # subcommands cannot start with -
                        help
                ;;
                *)
                        [ "$(type -t $1)" == "function" ] || help
                        $@ # run subcommand as function
                        ;;
        esac
}


run_subcommand $@

